package es.cells.groovy

@Grab("spring-boot-starter-web")
@Grab("groovy-templates")
@Grab("thymeleaf-spring4")
@Grab("org.grails:gorm-hibernate4-spring-boot:1.1.0.RELEASE")
@Grab("h2")

import grails.persistence.*
import org.springframework.http.*
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.boot.Banner

// static content in public folder
// css images javascript
@Controller
class StaticWeb {}

// for more info please visit
// https://spring.io/blog/2014/05/28/using-the-innovative-groovy-template-engine-in-spring-boot
@Controller
class TemplatesApp {
    @RequestMapping("/templates")
    def templateOne(){
        new ModelAndView("views/index",
        [bootVersion: Banner.package.implementationVersion, 
         groovyVersion: GroovySystem.version])
    }
}

@RestController
class GreetingController {

    @RequestMapping(
        value="/person/greet", 
        method = RequestMethod.GET)
    String greet(String firstName) {
        def p = Person.findByFirstName(firstName)
        return p ? "Hello ${p.firstName}!" : "Person not found"
    }

    @RequestMapping(
        value="/person/get/{firstName}",
        method = RequestMethod.GET)
    String getByFirstName(@PathVariable("firstName")String firstName) {
        def p = Person.findByFirstName(firstName)
        return p ? "Hello ${p.firstName}!" : "Person not found"
    }

    @RequestMapping(
        value = '/person/post',
        method = RequestMethod.POST)
    ResponseEntity addPerson(String firstName) {
        Person.withTransaction {
            def p = new Person(firstName: firstName).save()
            return new ResponseEntity( p ? HttpStatus.CREATED : HttpStatus.BAD_REQUEST)
        }
    }
}

@Entity
class Person {
    String firstName
}
