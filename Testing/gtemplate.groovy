@Grab("spring-boot-starter-web")

@Controller
class GTemplatesController {
  @RequestMapping("/gtemplate")
  def home() {
    new ModelAndView(
        "views/home",
        [bootVersion: org.springframework.boot.Banner.package.implementationVersion, 
         groovyVersion: GroovySystem.version])
  }
}
