# run the Hello webapp
JAVA_OPTS=-Xmx1024m spring run Hello.groovy -- --server.port=8000

# test the Hello webapp
spring test Hello.groovy TestHello.groovy

# run WebApp
spring jar WebApp.jar webapp.groovy User.groovy UserRepository.groovy
java -jar WebApp.jar

# run gormapp.groovy
spring run gormapp.groovy
get data >> curl http://localhost:8000/person/get/Alfonso
post data >> curl -d "firstName=Alfonso" http://localhost:8000/person/post/

# run with application-h2.properties add
spring run gormapp.groovy -- --spring.profiles.active=h2
and you get de "/h2/console" >> http://localhost:8000/h2/console/
h2-jdbc-url='jdbc:h2:mem:testdb'

# run with non default application properties
--spring.profiles.active=dev,hsqldb
