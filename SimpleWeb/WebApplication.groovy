package cells.es

@Grab('spring-boot-starter-web')
@Grab('spring-boot-starter-data-jpa')
@Grab("thymeleaf-spring4")
@Grab("mysql-connector-java")
@Grab("groovy-all")

import javax.persistence.Entity
import javax.persistence.Table
import javax.persistence.Id
import javax.persistence.Column
import javax.persistence.OneToOne
import javax.persistence.JoinColumn

import org.springframework.data.repository.*
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.Query;

import java.util.List
import groovy.json.JsonBuilder

public interface PersonRepository
    extends CrudRepository<Person, Integer> {
        Person findById(int id);
        Person findByLastname(String lastname)
        @Query("SELECT count(p.id) FROM Person p where p.id = :id")
        int check(@Param("id") int id);
    }

public interface UserRepository
    extends CrudRepository<User, Integer> {
        User findById(int id);
        User findByLogin(String login)
        User findByEmail(String email)
    }

@Entity
@Table(name="User")
class User{
    @Id
    @Column(name="Id")
    Integer id

    @Column(name="Login")
    String login
    @Column(name="Email")
    String email

    @OneToOne(optional=false)
    @JoinColumn(name="idPerson", unique=true, nullable=true)
    Person person

    public String toJson(){
        def json = new JsonBuilder()
        json {
            user{
                "Id"  this.id
                login this.login
                email this.email
                person{
                    idPerson this.person.id
                    firstName this.person.firstname
                    lastName this.person.lastname
                }
            }
        }
        return json.toPrettyString()
    }
}

@Entity
@Table(name="Person")
class Person{
    @Id
    @Column(name="idPerson")
    Integer id

    @Column(name="FirstName")
    String firstname
    @Column(name="LastName")
    String lastname

    @OneToOne(mappedBy="person")
    User user

    public String toJson(){
        def json = new JsonBuilder()
        json {
            person {
                "idPerson" this.id
                firstName this.firstname
                lastName this.lastname
                user{
                    id  this.user.id
                    login this.user.login
                    email this.user.email
                }
            }
        }
        return json.toPrettyString()
    }
}

// add public content
@Controller class StaticApp {}

@RestController
@EnableAutoConfiguration
class WebApplication {

    @Autowired PersonRepository prepo
    @Autowired UserRepository urepo

    @RequestMapping(
        value = "/user/{id}",
        method = RequestMethod.GET,
        produces = "application/json")
    String getUserById(
        @PathVariable("id") int id)
    {
        User u = urepo.findById(id)
        return u ? u.toJson() : "No User with ${id}"
    }

    @RequestMapping(
        value = "/user/login/{login}",
        method = RequestMethod.GET,
        produces = "application/json")
    String getUserByLogin(
        @PathVariable("login") String login)
    {
        User u = urepo.findByLogin(login)
        return u ? u.toJson() : "No User with login: ${login}"
    }

    @RequestMapping(
            value = "/user/email/{email}",
            method = RequestMethod.GET,
            produces = "application/json")
    String getUserByEmail(
            @PathVariable("email") String email)
    {
        User u = urepo.findByEmail(email)
        return u ? u.toJson() : "No User with email: ${email}"
    }

    @RequestMapping(
        value = "/person/{id}",
        method = RequestMethod.GET,
        produces = "application/json")
    String getPersonById(
        @PathVariable("id") int id)
    {
        Person p = prepo.findById(id)
        return p ? p.toJson() : "No Person with ${Id}"
    }

    @RequestMapping(
        value = "/person/lastname/{lastname}",
        method = RequestMethod.GET,
        produces = "application/json")
    String getPersonByLastName(
        @PathVariable("lastname") String lastname)
    {
        Person p = prepo.findByLastname(lastname)
        return p ? p.toJson() : "No Person with ${lastname}"
    }

    @RequestMapping(
        value = "/person/check/{id}",
        method = RequestMethod.GET,
        produces = "application/json")
    String checkPerson(
        @PathVariable("id") int id)
    {
        int c = prepo.check(id)
        return "${c} Person(s) with Id ${id}"
    }

}
