package es.cells

@Grab("org.grails:gorm-hibernate4-spring-boot:1.1.0.RELEASE")
@Grab('spring-boot-starter-data-jpa')
@Grab("mysql-connector-java")

import grails.persistence.*
import org.springframework.http.*
import static org.springframework.web.bind.annotation.RequestMethod.*

@RestController
class UserOffice{
    @RequestMapping(value="/user/{id}", method = GET)
    String getUser(@PathVariable("id")String id) {
        def u = User.findById(id)
        return u ? "Hello ${u.Login}!" : "User not found"
    }
    @RequestMapping(value="/person/{id}", method = GET)
    String getPerson(@PathVariable("id")String id) {
        def p = Person.findById(id)
        return p ? "Hello ${p.firstName}!" : "Person not found"
    }
}

@Entity
class User {
    String Email
    String Login
    Person person
}

@Entity
class Person {
    String firstName
    String lastName

    static mapping = {
        id column: 'idPerson'
        firstName column: 'FirstName'
        lastName column: 'LastName'
    }
}
