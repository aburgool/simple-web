import org.springframework.data.repository.Repository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends Repository<User, Long>{
    @Query("select u from User u where u.Id = ?")
    List<User> findById(int id);
}
