@Grab("spring-boot-starter-data-jpa")
@Grab("mysql-connector-java")
//@Grab("h2")

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.beans.factory.annotation.Autowired;


@RestController
class WebApp {

    User user
    @Autowired
    UserRepository urepo

    void init(){
        user = new User()
        user = urepo.findOne(1)
    }

    @RequestMapping(value="/", produces="text/plain")
    String webapp() {
        init()
        return "Hello " + user + " !"
    }

}
