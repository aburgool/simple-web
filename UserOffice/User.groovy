import javax.persistence.*
import org.springframework.data.repository.*;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Entity
public class User {
    @Id
    @Column
    Long id

    @Column
    String Login

    @Column
    String Email

    String toString(){
        return Login + ": " + Email
    }
}


