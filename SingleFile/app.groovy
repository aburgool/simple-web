package webapp

@Grab("spring-boot-starter-data-jpa")
@Grab("h2")


import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.ComponentScan
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

/*
@EnableAutoConfiguration
@ComponentScan
class Application {
    static main(args) {
        SpringApplication.run(Application.class, args)
    }
}*/



import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class Cat {
    @Id
    @GeneratedValue
    private Long id;

    public String message;

    def speak(){
        message
    }

}

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping

@Controller
class CatsController {

    @Autowired
    CatsRepository repository

    @RequestMapping('/cats/index')
    def index(Model model){
        def cats = repository.findAll()
        model.addAttribute("values", cats)
        "cats"
    }

}


import org.springframework.data.repository.CrudRepository;
public interface CatsRepository extends CrudRepository<Cat, Long> {
}
