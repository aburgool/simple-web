WiP

for JPA to work "out of the box" with groovy
A Groovy CLI app can only scan for JPA repositories if you give 
it actual classes (i.e. not the .groovy scripts). 
You can build a jar file and run that, and it should work:

spring jar app.jar app.groovy
java -jar app.jar

http://stackoverflow.com/questions/26639944/spring-data-jpa-repositories-not-being-auto-created-with-commandlinerunner


default server.port is 8000
templates are referenced by classpath:/templates/
